/*S24 Activity:

>> In the S19 folder, create an activity folder and an index.html and index.js file inside of it.

>> Link the script.js file to the index.html file.

>> Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)

*/

const getCube = 8 ** 3;
console.log("Result as per ES6 updates");
console.log(getCube);
//result: 512


/*

>> Using Template Literals, print out the value of the getCube variable with a message:
		 The cube of <num> is…


*/
message = (`The cube of <num> is… ${getCube}!`)
console.log(message)



/*

>> Destructure the given array and print out a message with the full address using Template Literals.
*/	
		const address = ["258", "Washington Ave NW", "California", "90011"];

		// message: I live at <details>


const [number, street, city, postalCode] = address ;

myAddress = (`I live at ${number} ${street} ${city} ${postalCode}`)
console.log(myAddress)



/*

>> Destructure the given object and print out a message with the details of the animal using Template Literals.
*/	
		const animal = {
			name: "Lolong",
			species: "saltwater crocodile",
			weight: "1075 kgs",
			measurement: "20 ft 3 in"
		};
/*
		message: <name> was a <species>. He weighed at <weight> with a measurement of <measurement>.*/




const {name, species, weight, measurement} = animal;


message2 = (`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}`)
console.log(message2)


/*

>> Loop through the given array of characters using forEach, an arrow function and using the implicit return statement to print out each character
*/		
		const characters = ['Ironman', 'Black Panther', 'Dr. Strange', 'Shang-Chi', 'Falcon']


		characters.forEach(characters => {
		    console.log(`${characters}`)
		});



/*

>> Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.


*/

class Dog{
	constructor(dogName, dogAge, breed){
		this.dogName = dogName;
		this.dogAge = dogAge;
		this.breed = breed;
	}
}


/*


>> Create/instantiate a 2 new object from the class Dog and console log the object*/



let myDog = new Dog();

myDog.dogName = 'Ein';
myDog.dogAge = 2;
myDog.breed = "Pembroke Welsh corgi";

console.log(myDog);

const newDog = new Dog("Den", 2, "Siberian Husky");
console.log(newDog);

